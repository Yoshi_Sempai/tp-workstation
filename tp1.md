## TP 1

## Host OS
* Nom de la machine
```
PS C:\Users\yoshi> Get-CimInstance -ClassName Win32_ComputerSystem
```
```
Name
MSI
```

* OS et version
````
PS C:\Users\yoshi> Get-CimInstance -ClassName Win32_OperatingSystem |
>>  Select-Object -Property BuildNumber,BuildType,OSType,ServicePackMajorVersion,ServicePackMinorVersion
````
````
18 
````
Puis pour la version


```
PS C:\Users\yoshi> Get-WmiObject Win32_OperatingSystem
```
````
10.0.18363
````

* Quantité RAM et modèle de la RAM
```
PS C:\Users\yoshi> Get-CimInstance -ClassName Win32_Processor | Select-Object -ExcludeProperty "CIM*"
```
````
Manufacturer Banklabel Configuredclockspeed Devicelocator    Capacity
------------ --------- -------------------- -------------    --------
Samsung      BANK 0                    2667 ChannelA-DIMM0 8589934592
Samsung      BANK 2                    2667 ChannelB-DIMM0 8589934592
````

## Devices

* La marque et le modèle de votre processeur
````
PS C:\Users\yoshi> Get-CimInstance -ClassName Win32_Processor | Select-Object -ExcludeProperty "CIM*"
````
````
DeviceID Name                                      Caption                               MaxClockSpeed SocketDesignatio
                                                                                                       n
-------- ----                                      -------                               ------------- ----------------
CPU0     Intel(R) Core(TM) i7-10750H CPU @ 2.60GHz Intel64 Family 6 Model 165 Stepping 2 2592          U3E1

````
* Identifier le nombre de processeurs, le nombre de coeur
````
CPU0  /  6
````
* Si c'est un proc Intel, expliquer le nom du processeur
````
Dixième génération, puis référence du processeur
````
* La marque et le modèle de votre touchpad/trackpad
````
Alors, comme je n'ai rien trouvé sur le sujet j'ai regardé dans mon gestionnaire périphériques: Souris compatible PS/2
````
* La marque et le modèle de votre carte graphique
````
PS C:\Users\yoshi> wmic path win32_VideoController get name
````
````
Name
NVIDIA GeForce RTX 2060
Intel(R) UHD Graphics
````
* Identifier la marque et le modèle de votre(vos) disque(s) dur(s)
````
PS C:\Users\yoshi> Get-PhysicalDisk
````
````
Number FriendlyName                   SerialNumber                             MediaType CanPool OperationalStatus Heal
                                                                                                                   thSt
                                                                                                                   atus
------ ------------                   ------------                             --------- ------- ----------------- ----
0      WDC PC SN730 SDBPNTY-1T00-1032 xxx. SSD       False   OK                H...

````
* Identifier les différentes partitions de votre/vos disque(s) dur(s)
````
PS C:\Users\yoshi> get-partition
````
````
PartitionNumber  DriveLetter Offset                                           Size Type
---------------  ----------- ------                                           ---- ----
1                            1048576                                        300 MB System
2                            315621376                                      128 MB Reserved
3                C           449839104                                   933.13 GB Basic
4                            1002387275776                                  900 MB Recovery
5                            1003330994176                                19.44 GB Recovery
````
* Déterminer le système de fichier de chaque partition
````
PS C:\Users\yoshi> diskpart
````
Puis
````
DISKPART> detail disk
````
````

WDC PC SN730 SDBPNTY-1T00-1032
ID du disque : {1D0AE9AD-03A9-45AE-BF60-9CE47DB8506F}
Type : NVMe
État : En ligne
Chemin : 0
Cible : 0
ID LUN : 0
Chemin d’accès de l’emplacement : PCIROOT(0)#PCI(1D00)#PCI(0000)#NVME(P00T00L00)
État en lecture seule actuel : Non
Lecture seule : Non
Disque de démarrage : Oui
Disque de fichiers d’échange : Oui
Disque de fichiers de mise en veille prolongée : Non
Disque de fichiers de vidage sur incident : Oui
Disque en cluster  : Non

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   Windows      NTFS   Partition    933 G   Sain       Démarrag
  Volume 1         SYSTEM       FAT32  Partition    300 M   Sain       Système
````
````
Partition démarrage : elle stocke les fichiers de démarrage et se trouvent en début de disque.
````
````
Partition système : La partition ou disque C sur Windows est la partition principale qui stocke : les dossiers et fichiers systèmes de Windows, les applications installées. les profils utilisateurs.
````

## Users

* la liste des utilisateurs de la machine
````
 PS C:\Users\yoshi> wmic useraccount list full
````
````
AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=MSI
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2051476266-2068326579-26886624-500
SIDType=1
Status=Degraded


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=MSI
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2051476266-2068326579-26886624-503
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=MSI
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2051476266-2068326579-26886624-501
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=MSI
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-2051476266-2068326579-26886624-504
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=MSI
FullName=diego philippe
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=yoshi
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2051476266-2068326579-26886624-1001
SIDType=1
Status=OK
````
* Déterminer le nom de l'utilisateur qui est full admin sur la machine
````
Name=Administrateur
````
## Processus
* Déterminer la liste des processus de la machine
* Choisissez 5 services système et expliquer leur utilité
````
Get-Service | Where{ $_.Status -eq "Running"
````
````
Running  AppXSvc            Service de déploiement AppX (AppXSVC)
Si j'ai bien compris c'est le processus qui gère les installations, les suppressions, les mises à jour et les licences des applications installées depuis le Windows Store.
````
````
Running  ProfSvc            Service de profil utilisateur
Le service ProfSvc est le service Windows qui gère le profil utilisateur. Il est impératif que ce service soit en cours de fonctionnement pour pouvoir ouvrir une session utilisateur.
````
````
Running  RpcEptMapper       Mappeur de point de terminaison RPC
Alors je vais être honnête, je n'ai pas totalement compris son utilitée mais je sais maintenant qu'énormément de services en dépendent. "Si ce service n'est pas disponible, le système d'exploitation ne se charge pas."
````
````
Running  Winmgmt            Infrastructure de gestion Windows
Fournit une interface commune et un modèle objet pour accéder aux informations de gestion du système d’exploitation, des périphériques, des applications et des services. Si ce service est arrêté, la plupart des logiciels sur base Windows ne fonctionneront pas correctement. Si ce service est désactivé, tout service qui en dépend explicitement ne démarrera pas.
````
````
Pour le dernier j'aurais aimé trouver la barre de tache, mais je n'y suis pas arrivé.
````
````
Du coup j'ai pris celui-là.
Running  LanmanWorkstation  Station de travail
Le service Station de travail ou LanmanWorkstation assure la prise en charge des connexions réseau sur les ordinateurs tournant sous Windows.
````
````
Je n'ai pas réussi à faire cette partie là.
````
## Network

* Afficher la liste des cartes réseau de votre machine
````
PS C:\Users\yoshi> Get-NetAdapter | fl Name
````
````
Name : Wi-Fi
Le nom parle de lui même. Ma carte wifi.
Name : Ethernet
Le nom de ma carte réseau filaire.
Name : Ethernet 2
Ethernet 2 est un protocole standard utilisé dans toutes les parties de l'équipement réseau, quel que soit le fabricant.
Name : NordLynx
NordLynx si j'ai bien compris est également un protocole mais qui cette fois  permet de """garantir la confidentialité des utilisateurs""", sur le wifi comme en filaire.
````
* Lister tous les ports TCP et UDP en utilisation
````
PS C:\Windows\system32> netstat -b
````
````
 Proto  Adresse locale         Adresse distante       État
  TCP    127.0.0.1:49705        MSI:51463              ESTABLISHED
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:51457        MSI:65001              ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:51463        MSI:49705              ESTABLISHED
 [NVIDIA Share.exe]
  TCP    127.0.0.1:65001        MSI:51457              ESTABLISHED
 [nvcontainer.exe]
  TCP    192.168.1.14:51925     162.159.135.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.14:51932     ec2-34-204-116-194:8884  ESTABLISHED
 [NordVPN.exe]
  TCP    192.168.1.14:51935     40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.1.14:51964     162.159.128.232:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.14:51966     a104-123-23-222:http   TIME_WAIT
  TCP    192.168.1.14:51967     40.90.137.124:https    ESTABLISHED
  wlidsvc
 [svchost.exe]
  TCP    192.168.1.14:51968     40.90.137.124:https    ESTABLISHED
  wlidsvc
  TCP    [2a01:cb19:649:2d00:2045:f4ae:6be4:e02d]:51924  wo-in-xbc:5228         ESTABLISHED
 [chrome.exe]
  TCP    [2a01:cb19:649:2d00:2045:f4ae:6be4:e02d]:51950  [2406:da14:88d:a101:eeaf:307e:51b:40d6]:https  ESTABLISHED
 [chrome.exe]
  TCP    [2a01:cb19:649:2d00:2045:f4ae:6be4:e02d]:51970  [2001:4de0:ac10::1:1:19]:http  ESTABLISHED
  CryptSvc
````
````
Vu que j'ai utilisé nestat -b les noms des programmes sont déjà affichés.
````
````
* NVIDIA Web Helper.exe : Le processus écoute ou envoie des données sur des ports ouverts à un réseau local ou à Internet. NVIDIA Web Helper.exe est capable de surveiller les applications et de manipuler d'autres programmes. La note de sécurité technique attribuée équivaut à 37% de dangerosité
````
````
* nvcontainer.exe : Ce n'est pas un fichier système de Windows. Le programme n'a pas de fenêtre visible. Le fichier a une signature numérique. Le processus utilise des ports pour se connecter à ou à partir d'un réseau local ou d'Internet. Nvcontainer.exe est capable de surveiller les applications. La note de sécurité technique attribuée équivaut à 41% de dangerosité, il est également lié à NVIDIA.
````
````
* NVIDIA Share.exe : Ce n'est pas un fichier système de Windows. Le programme n'est pas visible. NVIDIA Share.exe est capable d’enregistrer des entrées au clavier et à la souris, de manipuler d’autres programmes et de contrôler des applications. La note de sécurité technique attribuée équivaut à 15% de dangerosité
````
````
* Discord.exe : Discord est un programme VoIP multiplate-forme gratuit et populaire auprès des communautés de jeux en ligne. Discord.exe exécute Discord
````
````
* NordVPN.exe : L'application native NordVPN est l'option recommandée pour la connexion aux serveurs NordVPN sur votre PC Windows.
````
````
* svchost.exe : svchost.exe est un important processus Windows associé avec le service de systèmes Windows. Ce processus exécute des Bibliothèques de liens dynamiques
````
````
* SearchUI.exe : Le fichier authentique SearchUI.exe est un composant logiciel de Microsoft Windows de Microsoft . Microsoft Windows est un système d'exploitation. L'interface utilisateur de recherche est un processus faisant partie de l'assistant de recherche natif de Microsoft, Cortana.
````
````
* chrome.exe : Le fichier authentique chrome.exe est un composant logiciel de Google Chrome de Google. Chrome.exe est un fichier exécutable (un programme) pour Windows.
````

### A partir de là je suis passé sur ubuntu.


## Gestion des softs
Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
* Par rapport au téléchargement en direct sur internet
````
Le téléchargement est plus rapide, en terme d'APM, ou de débit de téléchargement, plus sécurisé pour le téléchargement ainsi que pour l'instalation car moins d'intermédiaires, chiffrement et vérification d'intégrité une fois téléchargé.
````
* Lister tous les paquets déjà installés
````
yoshi@yoshi-GL75-Leopard-10SER:~$ dpkg --get-selections
````
````
accountsservice					install
acl						install
acpi-support					install
acpid						install
adduser						install
adwaita-icon-theme				install
alsa-base					install
alsa-topology-conf				install
alsa-ucm-conf					install
alsa-utils					install
[...]
zenity-common					install
zip						install
zlib1g:amd64					install
````
* Déterminer la provenance des paquets
````
yoshi@yoshi-GL75-Leopard-10SER:~$ sudo cat /etc/apt/sources.list
````
````
deb-src http://dz.archive.ubuntu.com/ubuntu/
deb http://security.ubuntu.com/ubuntu
````

## Machine virtuelle


### 4. Partage de fichiers

* Créer un partage de fichiers SUR VOTRE PC (pas dans la VM) : 

J'ai choisis d'utiliser Samba car j'y vois une utilitée pour partagerdes fichiers/documents avec mon pc fixe qui est sous windows 10.

##### Partie serveur :

````
yoshi@yoshi-GL75-Leopard-10SER:~$ sudo apt install samba
````
Ensuite j'ai créé un fichier pour le partage :
````
mkdir /home/yoshi/sambashare/
````
Ensuite, le rendre partageable :
````
sudo nano /etc/samba/smb.conf
````
````
[sambashare]
    comment = Samba on Ubuntu
    path = /home/yoshi/sambashare
    read only = no
    browsable = yes
````
Si j'ai bien tout compris 
* comment bah c'est un commentaire ...
* path est le chemin vers sambashare
* read only correspond à l'autorisation de modifier le contenue et pas seulement de le lire, si read only est égale à ``no``
* Lorsqu’il est défini à yes, les gestionnaires de fichiers tels que le gestionnaire de fichiers par défaut d’Ubuntu listeront ce partage sous « Network » 

J'ai sauvegardé :
````
sudo service smbd restart
````
Puis mettre à jour les règles du pare-feu pour autoriser le trafic Samba :
````
sudo ufw allow samba
````
Définir un compte et un mot de passe :
````
sudo smbpasswd -a root
````
Et pour finir pour la partie serveur, j'ai connecté le "serveur"
![image](https://i.imgur.com/kb0Udif.png)

````
smb://192.168.1.14/sambashare/
````
##### Partie utilisateur :

Installer samba partie client
````
[root@localhost /]# yum -y install samba samba-client cifs-utils
````
![image2](https://i.imgur.com/tS02Dwu.png)

Un petit ``ls`` pour vérifier que mes fichier test soit bien là :

![image3](https://i.imgur.com/lUVTfdP.png)

